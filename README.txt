=== Plugin Name ===
Contributors: kartano
Donate link: https://kartano.net
Tags: monero, kartano, webminer, webmining, monetize, crypto,
Requires at least: 3.0.1
Tested up to: 4.8.9
Stable tag: 4.8
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html

This plugin add a monero webminer into your site. Allowing to monetize using the readers CPU

== Description ==

= KartanoMiner =

Monetize your blog by adding a monero webmining.
Kartano-webminer is a simple plugin that let you add a webmining script to your WordPress site.

The simple interface of the Kartano Webminer gives you one place where you can insert your monero wallet and select the throttle hash.

The hashes are sent to the mining pool www.supportxmr.com that will reward directly to your wallet.

= Features of KartanoMiner =

* Quick to set up
* Insert your wallet and set mining power

= Credits =

This plugin is created by <a href="https://kartano.net/" rel="friend" title="Kartano">Kartano</a>.

= What's Next =

If you want to see an example check here:

* <a href="https://photo.kartano.net/" rel="friend" title="piture of my mind">pictures of my mind</a>

== Installation ==

1. Install Kartano Miner by uploading the `kartanominer` directory to the `/wp-content/plugins/` directory. (See instructions on <a href="http://www.wpbeginner.com/beginners-guide/step-by-step-guide-to-install-a-wordpress-plugin-for-beginners/" rel="friend">how to install a WordPress plugin</a>.)
2. Activate Kartano Miner through the `Plugins` menu in WordPress.
3. Insert your monero wallet address by going to the `Settings > Kartano Miner menu.

== Screenshots ==

1. Settings Screen

== Frequently Asked Questions ==

= Can I mine direclty to my mining pool =

No, not yet

= What is the mining fee? =

10.4%


== Notes ==
Kartano Miner is an easy way to monetize through your user visits.


Thank you
Kartano team

== Changelog ==


= 0.0.1 =
* Initial version
