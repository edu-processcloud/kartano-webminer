<?php

/**
 * Provide a admin area view for the plugin
 *
 * This file is used to markup the admin-facing aspects of the plugin.
 *
 * @link       https://kartano.net
 * @since      1.0.0
 *
 * @package    Kartanominer
 * @subpackage Kartanominer/admin/partials
 */
?>

<!-- This file should primarily consist of HTML with a little bit of PHP. -->
<div class="wrap">

    <h2><?php echo esc_html(get_admin_page_title()); ?></h2>

    <form method="post" name="kartanominer" action="options.php">

      <?php

             //Grab all options
             $options = get_option($this->plugin_name);

             $wallet = $options['wallet'];
             settings_fields($this->plugin_name);

             do_settings_sections($this->plugin_name);
             ?>
        <fieldset>
            <legend class="screen-reader-text"><span>Monero wallet</span></legend>
            <label for="<?php echo $this->plugin_name; ?>-wallet">
                <input type="text" id="<?php echo $this->plugin_name; ?>-wallet" name="<?php echo $this->plugin_name; ?>[wallet]" value="<?php echo $wallet; ?>" maxlength="95"/>
                <span><?php esc_attr_e('Enter monero wallet address', $this->plugin_name); ?></span>
            </label>
        </fieldset>

        <?php submit_button('Save all changes', 'primary','submit', TRUE); ?>

    </form>

</div>
