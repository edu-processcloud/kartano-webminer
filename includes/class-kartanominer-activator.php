<?php

/**
 * Fired during plugin activation
 *
 * @link       https://kartano.net
 * @since      1.0.0
 *
 * @package    Kartanominer
 * @subpackage Kartanominer/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Kartanominer
 * @subpackage Kartanominer/includes
 * @author     @kartano <social@kartano.net>
 */
class Kartanominer_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
