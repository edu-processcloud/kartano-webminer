<?php

/**
 * Fired during plugin deactivation
 *
 * @link       https://kartano.net
 * @since      1.0.0
 *
 * @package    Kartanominer
 * @subpackage Kartanominer/includes
 */

/**
 * Fired during plugin deactivation.
 *
 * This class defines all code necessary to run during the plugin's deactivation.
 *
 * @since      1.0.0
 * @package    Kartanominer
 * @subpackage Kartanominer/includes
 * @author     @kartano <social@kartano.net>
 */
class Kartanominer_Deactivator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function deactivate() {

	}

}
